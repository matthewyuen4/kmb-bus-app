import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet"
import "../../node_modules/leaflet/dist/leaflet.css";

export default function StopDetails({ stopDetails, returnB }: any) {
    let data = { stopDetails, returnB }
    let busStopDetails = data.stopDetails
    console.log("stop details:", busStopDetails)
    // let stopDetails = earlyBus[busIndex]

    function getEtaMin(eta: any) {
        let getNowDateMs = Date.now()
        let dateEta = new Date(eta)
        let etaMs = dateEta.getTime()

        let timeDiff = (getNowDateMs) - etaMs

        let etaMin = Math.floor(Math.abs(timeDiff) / 1000 / 60)
        if (etaMin < 1000) {
            return etaMin
        } else {
            return "-"
        }
    }

    return (
        <div className="busDetails_layout">
            <div className="right_bus_map">
                <MapContainer center={[busStopDetails[0].lat, busStopDetails[0].long]} zoom={18} scrollWheelZoom={false}>
                    <TileLayer
                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    <Marker position={[busStopDetails[0].lat, busStopDetails[0].long]}>
                        <Popup>
                            {busStopDetails[0].stop}
                        </Popup>
                    </Marker>
                </MapContainer>
            </div>
            <div className="right_bus_details">
                <h3>{busStopDetails[0].stop}</h3>
                {busStopDetails.map((busDetails: any, index: any) => (
                    <div key={index}>{getEtaMin(busDetails.eta)}<span> 分鐘</span></div>
                ))}
            </div>
            <button onClick={() => data.returnB()}>return</button>
        </div>
    )
}