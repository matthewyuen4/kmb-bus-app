import BusList_details from "./BusList_details";
import PullToRefresh from 'react-simple-pull-to-refresh';
import { useEffect, useState } from "react";

export default function BusList() {

    const [busId, setBusId] = useState(["BFA3460955AC820C", "5FB1FCAF80F3D97D"])
    const [busListDetails, setListBusDetails] = useState([{}])
    const [boolean, setBoolean] = useState(false)

    async function fetchData(busIds: Array<string>) {
        let busStopId = busIds
        let res_busEta1 = await fetch(
            `https://data.etabus.gov.hk/v1/transport/kmb/stop-eta/${busStopId[0]}`
        )
        let res_busEta2 = await fetch(
            `https://data.etabus.gov.hk/v1/transport/kmb/stop-eta/${busStopId[1]}`
        )
        let res_busStop1 = await fetch(
            `https://data.etabus.gov.hk/v1/transport/kmb/stop/${busStopId[0]}`
        )
        let res_busStop2 = await fetch(
            `https://data.etabus.gov.hk/v1/transport/kmb/stop/${busStopId[1]}`
        )
        let data_busEta1 = await res_busEta1.json();
        let data_busEta2 = await res_busEta2.json();
        let data_busStop1 = await res_busStop1.json();
        let data_busStop2 = await res_busStop2.json();

        let busList_data1 = data_busEta1.data.map((obj: any) => ({ ...obj, stop: data_busStop1.data.name_tc, lat: data_busStop1.data.lat, long: data_busStop1.data.long }))
        let busList_data2 = data_busEta2.data.map((obj: any) => ({ ...obj, stop: data_busStop2.data.name_tc, lat: data_busStop2.data.lat, long: data_busStop2.data.long }))
        let mixedData = busList_data1.concat(busList_data2)

        setListBusDetails(mixedData)
    }

    useEffect(() => {
        fetchData(busId)
    }, [])


    return (
        <div className="busList-layout">
            <PullToRefresh onRefresh={() => fetchData(busId)}>
                <BusList_details setListBusDetails={setListBusDetails} busListDetails={busListDetails} />
            </PullToRefresh>
        </div>
    )
}

