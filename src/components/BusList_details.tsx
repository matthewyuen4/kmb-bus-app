import { useEffect, useState } from "react"
import StopDetails from "./StopDetails"

export default function BusList_details(setListBusDetails: any, busListDetails: any) {


    const [status, setStatus] = useState(true)
    const [earlyBus, setEarlyBus] = useState([{}])
    const [selectedBusEta, setSelectedBusEta] = useState([{}])

    // console.log({ busStop })
    // console.log({ earlyBus })

    async function getEarlyBus() {
        let data = setListBusDetails.busListDetails
        console.log({ data })
        let earlyBus = await reFormateData(data)
        setEarlyBus(earlyBus)
    }

    async function reFormateData(data: Array<any>) {
        let formateData = []
        for (let i = data.length - 1; i >= 0; i--) {
            if ((i > 0) && (data[i]["route"] == data[i - 1]["route"])) {
                continue
            } else {
                formateData.push(data[i])
            }
        }
        return formateData
    }

    function getEtaMin(eta: any) {
        console.log({ eta })
        let getNowDateMs = Date.now()
        let dateEta = new Date(eta)
        let etaMs = dateEta.getTime()

        let timeDiff = getNowDateMs - etaMs

        let etaMin = Math.floor(Math.abs(timeDiff) / 1000 / 60)
        if (eta !== null) {
            return etaMin
        } else {
            return "-"
        }
    }

    async function getBusStopEta(busDetails: any) {
        let busStopData = { busRoute: busDetails.route, busStop: busDetails.stop }
        if ((busStopData.busRoute !== "") && (busStopData.busStop !== "")) {
            let selectedBusEtaArray = []
            let data = setListBusDetails.busListDetails
            for (let bus of data) {
                if ((bus["route"] == busStopData.busRoute) && (bus["stop"] == busStopData.busStop)) {
                    selectedBusEtaArray.push(bus)
                } else {
                    continue
                }
            }
            setSelectedBusEta(selectedBusEtaArray)
        }
        setStatus(false)
    }

    let returnB = () => { setStatus(true) }


    useEffect(() => {
        getEarlyBus()
    }, [setListBusDetails])

    if (status == true) return (
        <div>
            {earlyBus.map((busDetails: any, index: any) => (
                <div key={index} className="bus_details" onClick={() => { getBusStopEta(busDetails) }}>
                    <div>
                        {busDetails.route}
                    </div>
                    <div>
                        <span>往</span>{busDetails.dest_tc}
                        <div>{busDetails.stop}</div>
                    </div>
                    <div>
                        {getEtaMin(busDetails.eta)}
                        <div>分鐘</div>
                    </div>
                </div>
            ))}
        </div>
    )
    if (status == false) return (<StopDetails stopDetails={selectedBusEta} returnB={returnB} />)

    return <div>No result</div>
}


