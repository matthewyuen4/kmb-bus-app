import './App.scss';
import BusList from './components/BusList';

function App() {
  return (
    <div className="App">
      <div className="layout">
        <BusList />
      </div>
    </div>
  );
}

export default App;
