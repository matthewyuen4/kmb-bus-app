## a. Installation and execution steps 

##### git clone from git lab
1. open a folder with any name
2. open terminal of that folder location 
3. input `git clone "https://gitlab.com/matthewyuen4/kmb-bus-app.git"`
4. input `cd kmb-bus-app`

##### a) install library and run by npm
5. `npm install`
6. `npm start`
##### b) install library and run by yarn
5. `yarn install`
6. `yarn start`
        
## b. Assumptions you have taken into account 

1) The app only supports two bus stop

## c. Choice of solutions and justification
    
For the Map part, I have chosen OpenStreetMap React Leaflet instead of google map, as it is a open-source library that does not require register and credit-card information.
